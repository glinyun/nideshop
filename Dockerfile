# Version 0.1

# 基础镜像
FROM node:8-alpine

# 维护者信息
LABEL maintainer="ucheng.chian@gmail.com"

# 镜像操作命令
RUN npm install -g --registry=https://registry.npm.taobao.org pm2@2.10.1 thinkjs@3.2.7
RUN apk --update add git openssh

RUN mkdir /var/www
RUN cd /var/www
WORKDIR /var/www

RUN mkdir -p /var/www/nideshop
COPY . /var/www/nideshop/
RUN rm -rf /var/www/nideshop/.vscode/
RUN rm -rf /var/www/nideshop/.git*
RUN rm -f /var/www/nideshop/.eslintrc
RUN rm -f /var/www/nideshop/Dockerfile*
RUN rm -f /var/www/nideshop/nginx.conf
RUN rm -f /var/www/nideshop/nideshop.sql
RUN rm -f /var/www/nideshop/app/

RUN cd /var/www/nideshop
WORKDIR /var/www/nideshop

RUN mkdir -p /var/www/nideshop/www/static/upload/brand/
RUN mkdir -p /var/www/nideshop/www/static/upload/category/
RUN mkdir -p /var/www/nideshop/www/static/upload/topic/
RUN mkdir -p /var/www/nideshop/www/static/user/

RUN npm install --registry=https://registry.npm.taobao.org
RUN npm run compile

# 容器启动命令
EXPOSE 8360
# ENTRYPOINT [ "node", "production.js" ]
CMD [ "npm", "start" ]
# CMD ["pm2", "start", "pm2.json"]
